# Conky - Modern Clock

Interpretation of [Modern Clock for KDE](https://github.com/Prayag2/kde_modernclock) for [Conky](https://github.com/brndnmtthws/conky).

## Requirements

Obviously you will need to have ```conky``` installed, additionally you will need an installation of ```lua```.

The configuration makes use of the two fonts [Anurati](https://befonts.com/anurati-font.html) and [Poppins](https://fonts.google.com/specimen/Poppins) which you have to download and install.


### Font Installation

After extracting the archives move the ```.ttf``` and/or ```.otf``` files to your fonts directory:

```shell
mv *.{ttf,otf} ${HOME}/.local/share/fonts
```

Afterwards you will have to rebuild the fonts cache:

```shell
fc-cache -vf
```

## Installation

1. Cloning this repository

```shell
git clone https://codeberg.org/stoerdebegga/conky.git ${HOME}/.conky
```

2. Setting a symlink to the conkyrc file

```shell
ln -s ${HOME}/.conky/ModernClock/modernclock.conkyrc ${HOME}/.conkyrc
```

Afterwards you can start conky.

## Screenshot
![Modern Clock Screenshot](.screenshots/modernclock.png)

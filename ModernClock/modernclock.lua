-- Time 2 Words
-- Literal representation of time
--
-- @author stoerdebegga <stoerdebegga@mailbox.org>
--
local function modernclock(component)
    local cc
    if component == "datetime" then
        cc = string.format("%s // %s", os.date("%d %b %Y"), os.date("%I:%M %p"))
    elseif component == "day" then
        cc = string.format("%s", os.date("%A"))
    else
        cc = "nope"
    end

    return string.upper(cc)
end

local linebreak = "\n"
print(modernclock(arg[1]) .. linebreak)

# Conky - QlockTwo

Adaption of [Qlocktwo](https://qlocktwo.com/) for [Conky](https://github.com/brndnmtthws/conky).

## Requirements

Obviously you will need to have ```conky``` installed, additionally you will need an installation of ```lua```.

The configuration makes use of the font [Fira Mono](https://fonts.google.com/specimen/Fira+Mono) which you have to download and install.

### Font Installation

After extracting the archive move the ```.otf``` files to your fonts directory:

```shell
mv *.otf ${HOME}/.local/share/fonts
```

Afterwards you will have to rebuild the fonts cache:

```shell
fc-cache -vf
```

## Installation

1. Cloning this repository

```shell
git clone https://codeberg.org/stoerdebegga/conky.git ${HOME}/.conky
```

2. Setting a symlink to the conkyrc file

```shell
ln -s ${HOME}/.conky/QlockTwo/qlocktwo.conkyrc ${HOME}/.conkyrc
```

Afterwards you can start conky.

## Screenshot
![QlockTwo Clock Screenshot](.screenshots/qlocktwo.png)

# Conky Clocks

[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)

Some variations of clocks for [Conky](https://github.com/brndnmtthws/conky).

* [ModernClock](ModernClock/README.md) - Interpretation of [Modern Clock for KDE](https://github.com/Prayag2/kde_modernclock)
* [Time2Words](Time2Words/README.md) -  A textual representation of time
* [QlockTwo](QlockTwo/README.md) - Adaption of [Qlocktwo](https://qlocktwo.com/)

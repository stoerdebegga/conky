-- Time 2 Words
-- Literal representation of time
--
-- @author stoerdebegga <stoerdebegga@mailbox.org>
--
local function simpleclock(component)
    local cc
    if component == "time" then
        cc = string.format("%s", os.date("%I:%M %p"))
    elseif component == "date" then
        cc = string.format("%s", os.date("%b %d %Y"))
    elseif component == "day" then
        cc = string.format("%s", os.date("%A"))
    else
        cc = "nope"
    end

    return string.upper(cc)
end

local linebreak = "\n"
print(simpleclock(arg[1]) .. linebreak)

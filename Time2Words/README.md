# Conky - Time2Words

A textual representation of time.

## Requirements

Obviously you will need to have ```conky``` installed, additionally you will need an installation of ```lua```.

The configuration makes use of the font [Gidole](https://www.fontsquirrel.com/fonts/gidole) which you have to download and install.

### Font Installation

After extracting the archive move the ```.ttf``` file to your fonts directory:

```shell
mv *.ttf ${HOME}/.local/share/fonts
```

Afterwards you will have to rebuild the fonts cache:

```shell
fc-cache -vf
```

## Installation

1. Cloning this repository

```shell
git clone https://codeberg.org/stoerdebegga/conky.git ${HOME}/.conky
```

2. Setting a symlink to the conkyrc file

```shell
ln -s ${HOME}/.conky/Time2Words/time2words.conkyrc ${HOME}/.conkyrc
```

Afterwards you can start conky.

## Screenshot
![Time2Words Clock Screenshot](.screenshots/time2words.png)

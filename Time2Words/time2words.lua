-- Time 2 Words
-- Literal representation of time
--
-- @author stoerdebegga <stoerdebegga@mailbox.org>
--
local function time2words(hour, minute)
    local wm
    local words = {}
    words[0] = "Oh"
    words[1] = "One"
    words[2] = "Two"
    words[3] = "Three"
    words[4] = "Four"
    words[5] = "Five"
    words[6] = "Six"
    words[7] = "Seven"
    words[8] = "Eight"
    words[9] = "Nine"
    words[10] = "Ten"
    words[11] = "Eleven"
    words[12] = "Twelve"
    words[13] = "Thirteen"
    words[14] = "Fourteen"
    words[15] = "Fifteen"
    words[16] = "Sixteen"
    words[17] = "Seventeen"
    words[18] = "Eighteen"
    words[19] = "Nineteen"
    words[20] = "Twenty"
    words[30] = "Thirty"
    words[40] = "Fourty"
    words[50] = "Fifty"
    words[99] = "O'Clock"

    if minute == 0 then
        wm = words[99]
    elseif minute < 10 then
        wm = words[0] .. " " .. words[minute]
    elseif minute <= 20 or minute == 30 or minute == 40 or minute == 50 then
        wm = words[minute]
    else
        for i = 50, 20, -10 do
            if minute > i then
                local fraction = minute - i
                wm = words[i] .. " " .. words[fraction]
                break
            end
        end
    end

    return words[hour] .. "\n" .. wm
end

print(time2words(tonumber(os.date("%I")), tonumber(os.date("%M"))))
